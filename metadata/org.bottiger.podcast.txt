Categories:Multimedia
License:GPLv3
Web Site:https://github.com/bottiger/SoundWaves/blob/HEAD/Readme.md
Source Code:https://github.com/bottiger/SoundWaves
Issue Tracker:https://github.com/bottiger/SoundWaves/issues
Changelog:https://github.com/bottiger/SoundWaves/blob/HEAD/Changelog.txt

Auto Name:SoundWaves
Summary:Manage and listen to podcasts
Description:
Manage, fetch and listen to podcasts.
.

Repo Type:git
Repo:https://github.com/bottiger/SoundWaves

Build:0.69,83
    disable=fail at dex
    commit=2ef1cfcff49916cb72f8acc4f088e762dd09a973
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' -e '/jsoup/afreeCompile "ch.acra:acra:4.5.0"' build.gradle

Build:0.70,84
    commit=v0.84
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.72,86
    commit=86
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.74,88
    commit=88
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.76,92
    commit=92
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.78.1,96
    commit=96
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.78.2,98
    commit=98
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.80,100
    commit=100
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.82,102
    commit=102
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.83,103
    commit=104
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.86,108
    commit=108
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.88,110
    commit=110
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.88.4,114
    commit=114
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.90,116
    commit=116
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.92,123
    commit=123
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.94,126
    commit=126
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.94.2,130
    commit=130
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.94.4,132
    commit=132
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.96,134
    commit=134
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.96.2,136
    commit=136
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.96.4,138
    commit=138
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.98,144
    disable=proguard errors
    commit=144
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.98.2,148
    commit=148
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.100,150
    disable=broken
    commit=150
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.102,154
    commit=154
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.104,158
    commit=158
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.106,168
    commit=76a4c6e1d9444cbe910d398dea91f16f8c2b6def
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.108,170
    commit=170
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.108.2,172
    commit=172
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.108.8,178
    commit=178
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.108.10,180
    commit=180
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.110.2,188
    disable=gradle errors since com.android.databinding:library:1.0-rc2 cannot be found
    commit=188
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.112,192
    commit=192
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.114,200
    commit=200
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.116.4,206
    commit=206
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.118.7,215
    commit=214
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.118.10,218
    commit=218
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.122,222
    disable=prerelease gradle plugin
    commit=222
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.126,226
    disable=pre-release gradle
    commit=226
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128,230
    commit=230
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.2,232
    commit=232
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.4,234
    commit=234
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.12,244
    disable=java build fails
    commit=244
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.12,246
    commit=246
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.14,248
    commit=248
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.16,252
    commit=252
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.18,256
    commit=256
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.22,260
    commit=260
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.128.24,262
    commit=262
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.130,268
    disable=nonexisting tag
    commit=268
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.132,272
    commit=272
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.136,280
    commit=280
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.136.2,282
    commit=282
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.136.4,284
    commit=284
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.136.6,286
    commit=286
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.136.10,292
    commit=292
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Auto Update Mode:Version %c
Update Check Mode:Tags
Current Version:0.136.10
Current Version Code:292
