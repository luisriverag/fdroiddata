Disabled:not yet ready
Categories:Internet
License:Apache2
Web Site:https://zom.im/
Source Code:https://github.com/zom/zom-android
Issue Tracker:https://github.com/zom/zom-android/issues
Changelog:https://github.com/zom/Zom-Android/blob/HEAD/CHANGELOG

Auto Name:Zom
Summary:Text with friends
Description:
Mobile messenger with focus on ease-of-use and security. Zom is the new way to
gather together with your friends on the go.

* Easy Setup. Zom helps you create an account quickly and find ways to connect with your friends.
* Totally Free. No costs for messages, and no limits on what you say, or who you can talk to.
* Great Sharing. Easily send voice messages, share photos, send stickers and more.
* (Unencrypted) Group Chat. Create chats and invite your friends.
* Multiple Accounts. You can create and use different identities for home, work, family and more.
* Any Server, Any Where. Unlike other apps that keep you stuck in their walled garden, Zom is fully interoperable with any app that supports OTR and XMPP, such as ChatSecure, Conversations, Adium, Jitsi, and more.
.

Repo Type:git
Repo:https://github.com/zom/zom-android
Binaries:https://zom.im/download/Zom-%v.apk

Build:15.0.1-BETA-8,1501908
    disable=builds, but doesnt verify; also binary url needs to be fixed by upstream
    commit=15.0.1-BETA-8
    subdir=app
    submodules=yes
    gradle=zomrelease
    prebuild=sed -i '/com.google.android.gms/d' ../chatsecure-push/example/build.gradle

Build:15.0.1-BETA-10,1501910
    commit=15.0.1-BETA-10
    subdir=app
    submodules=yes
    gradle=zomrelease
    prebuild=sed -i '/com.google.android.gms/d' ../chatsecure-push/example/build.gradle

Build:15.0.1-RC-1,1501911
    commit=15.0.1-RC-1
    subdir=app
    submodules=yes
    gradle=zomrelease
    prebuild=sed -i '/com.google.android.gms/d' ../chatsecure-push/example/build.gradle

Build:15.0.1-RC-2a,1501921
    commit=15.0.1-RC-2a
    subdir=app
    submodules=yes
    gradle=zomrelease
    prebuild=sed -i '/com.google.android.gms/d' ../chatsecure-push/example/build.gradle

Build:15.0.1-RC-3,1501923
    commit=15.0.1-RC-3
    subdir=app
    submodules=yes
    gradle=zomrelease
    prebuild=sed -i '/com.google.android.gms/d' ../chatsecure-push/example/build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:15.0.1-RC-3
Current Version Code:1501923
