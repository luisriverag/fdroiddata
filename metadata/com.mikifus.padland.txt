Categories:Writing
License:Apache2
Web Site:https://github.com/mikifus/padland/blob/HEAD/README.md
Source Code:https://github.com/mikifus/padland
Issue Tracker:https://github.com/mikifus/padland/issues

Auto Name:Padland
Summary:Etherpad manager
Description:
Padland is a tool to manage, share, remember and read collaborative documents
based on the Etherpad technology in Android.
.

Repo Type:git
Repo:https://github.com/mikifus/padland.git

Build:1.1,2
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.1.1,3
    commit=v1.1.1
    subdir=app
    gradle=yes

Build:1.1.2,4
    commit=v1.1.2
    subdir=app
    gradle=yes

Build:1.1.3,5
    commit=v1.1.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.3
Current Version Code:5
